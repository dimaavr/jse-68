package ru.tsc.avramenko.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.*;
import ru.tsc.avramenko.tm.listener.AbstractListener;
import ru.tsc.avramenko.tm.listener.system.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.UnknownCommandException;
import ru.tsc.avramenko.tm.util.TerminalUtil;
import ru.tsc.avramenko.tm.util.SystemUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    private IListenerService listenerService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Autowired
    private ApplicationEventPublisher publisher;


    public void start(String[] args) {
        displayWelcome();
        initCommands();
        initPID();
        parseArgs(args);
        process();
    }

    @SneakyThrows
    private void initCommands() {
        if (abstractListeners == null) return;
        Arrays.stream(abstractListeners).filter(Objects::nonNull).forEach(t -> listenerService.add(t));
    }

    @SneakyThrows
    private void initPID() {
            @NotNull final String filename = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **"+"\n");
    }

    private void commandCompleted(@Nullable final String command) {
        if (System.console() == null) {
            System.out.println("\n" + TerminalUtil.ANSI_GREEN + "Command '" + command + "' completed." + TerminalUtil.ANSI_RESET + "\n");
        }
        else {
            System.out.println("\n" + "Command '" + command + "' completed." + "\n");
        }
    }

    private void displayAuth() {
        if (System.console() == null) {
            System.out.println(TerminalUtil.ANSI_RED + TerminalUtil.AUTHORIZED + TerminalUtil.ANSI_RESET);
        }
        else {
            System.out.println(TerminalUtil.AUTHORIZED);
        }
        System.out.print("\n");
        System.out.println(listenerService.getCommandByName("login"));
        System.out.println(listenerService.getCommandByName("exit"));
        System.out.println(listenerService.getCommandByName("user-create"));
        System.out.println(listenerService.getCommandByName("about"));
        System.out.println(listenerService.getCommandByName("version"));
        System.out.print("\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitListener().name();
        @NotNull
        String command = "";
        fileScanner.init();
        while (!exit.equals(command)) {
            try {
                while (sessionService.getSession() == null) { //доступные команды для неавторизированных пользователей
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit":
                            case "login":
                            case "user-create":
                            case "about":
                            case "version": parseCommand(command); break;
                            case "": throw new UnknownCommandException();
                            default: throw new AccessDeniedException();
                        }
                    } catch (@NotNull final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
                commandCompleted(command);
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
        fileScanner.stop();
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable
        AbstractListener arguments = listenerService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        publisher.publishEvent(new ConsoleEvent(arguments.name()));
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractListener abstractListener = listenerService.getCommandByName(command);
        if (!Optional.ofNullable(abstractListener).isPresent()) throw new UnknownCommandException();
        publisher.publishEvent(new ConsoleEvent(command));
        if (Thread.currentThread().getName().equals("ScannerThread")) {
            commandCompleted(command);
            System.out.println("ENTER COMMAND:");
        }
    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}