package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;

public interface ITaskService {

    @Nullable Task findById(@Nullable String id);

    @Nullable Collection<Task> findAll();

    void clear();

    @NotNull Task create(@Nullable Task task);

    void removeById(@Nullable String id);

    @NotNull Task save(@NotNull Task task);

}