package ru.tsc.avramenko.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

}